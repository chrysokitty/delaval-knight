import React from 'react';
import { graphql } from "gatsby";

import Layout from "../layouts/general";

import Wedding from '../content/wedding';

const Metadata = {
  title: 'Delaval Knight - Wedding',
  description: 'Delaval Knight - Description',
};

export default function Index({ data }){
  return (
    <Layout metadata={Metadata}>
      <Wedding imageData={data.wedding} />
    </Layout>
  );
};

export const query = graphql`
  query {
    wedding: file(relativePath: { eq: "wedding.jpg" }) {
      childImageSharp {
        fixed(width: 650, height: 461, quality: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`;

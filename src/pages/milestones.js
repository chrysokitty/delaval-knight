import React from 'react';
import { graphql } from "gatsby";

import Layout from "../layouts/general";

import Milestones from '../content/milestones';

const Metadata = {
  title: 'Delaval Knight - Milestones',
  description: 'Delaval Knight - Description',
};

export default function Index({ data }){
  return (
    <Layout metadata={Metadata}>
      <Milestones imageData={data.milestones} />
    </Layout>
  );
};

export const query = graphql`
  query {
    milestones: file(relativePath: { eq: "milestones.jpg" }) {
      childImageSharp {
        fixed(width: 1000, height: 709, quality: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`;

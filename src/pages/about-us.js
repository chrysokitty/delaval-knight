import React from 'react';
import { graphql } from "gatsby";

import Layout from "../layouts/general";

import AboutUs from '../content/about-us';

const Metadata = {
  title: 'Delaval Knight - About us',
  description: 'Delaval Knight - Description',
};

export default function Index({ data }){
  return (
    <Layout metadata={Metadata}>
      <AboutUs imageData={data.del} />
    </Layout>
  );
};

export const query = graphql`
  query {
    del: file(relativePath: { eq: "del.jpeg" }) {
      childImageSharp {
        fixed(width: 600, height: 850, quality: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`;

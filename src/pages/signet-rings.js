import React from 'react';
import { graphql } from "gatsby";

import Layout from "../layouts/general";

import SignetRings from '../content/signet_rings';

const Metadata = {
  title: 'Delaval Knight - Signet Rings',
  description: 'Delaval Knight - Description',
};

export default function Index({ data }){
  return (
    <Layout metadata={Metadata}>
      <SignetRings imageData={data.signetRings} />
    </Layout>
  );
};

export const query = graphql`
  query {
    signetRings: file(relativePath: { eq: "signet-rings.jpg" }) {
      childImageSharp {
        fixed(width: 650, height: 461, quality: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`;

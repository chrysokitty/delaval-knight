import React from 'react';
import { graphql } from "gatsby";

import Layout from "../layouts/general";

import Bespoke from '../content/bespoke';

const Metadata = {
  title: 'Delaval Knight',
  description: 'Delaval Knight - Description',
};

export default function Index({ data }){
  return (
    <Layout metadata={Metadata}>
      <Bespoke imageData={data.bespoke} />
    </Layout>
  );
};

export const query = graphql`
  query {
    bespoke: file(relativePath: { eq: "bespoke.jpg" }) {
      childImageSharp {
        fixed(width: 1050, quality: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`;

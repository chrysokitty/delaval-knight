import React from 'react';
import { graphql } from "gatsby";

import Layout from "../layouts/general";

import Cufflinks from '../content/cufflinks';

const Metadata = {
  title: 'Delaval Knight - Cufflinks',
  description: 'Delaval Knight - Description',
};

export default function Index({ data }){
  return (
    <Layout metadata={Metadata}>
      <Cufflinks imageData={data.cufflinks} />
    </Layout>
  );
};

export const query = graphql`
  query {
    cufflinks: file(relativePath: { eq: "cufflinks.jpg" }) {
      childImageSharp {
        fixed(width: 650, height: 461, quality: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`;

import React from 'react';
import { graphql } from "gatsby";

import Layout from "../layouts/general";

import Engagement from '../content/engagement';

const Metadata = {
  title: 'Delaval Knight - Engagement',
  description: 'Delaval Knight - Description',
};

export default function Index({ data }){
  return (
    <Layout metadata={Metadata}>
      <Engagement imageData={data.engagement} />
    </Layout>
  );
};

export const query = graphql`
  query {
    engagement: file(relativePath: { eq: "engagement.jpg" }) {
      childImageSharp {
        fixed(width: 650, height: 461, quality: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`;


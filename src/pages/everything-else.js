import React from 'react';
import { graphql } from "gatsby";

import Layout from "../layouts/general";

import EverythingElse from '../content/everything_else';

const Metadata = {
  title: 'Delaval Knight - Everything Else',
  description: 'Delaval Knight - Description',
};

export default function Index({ data }){
  return (
    <Layout metadata={Metadata}>
      <EverythingElse imageData={data.everythingElse} />
    </Layout>
  );
};

export const query = graphql`
  query {
    everythingElse: file(relativePath: { eq: "everything-else.jpg" }) {
      childImageSharp {
        fixed(width: 650, height: 461, quality: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`;

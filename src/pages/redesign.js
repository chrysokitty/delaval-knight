import React from 'react';
import { graphql } from "gatsby";

import Layout from "../layouts/general";

import Redesign from '../content/redesign';

const Metadata = {
  title: 'Delaval Knight - Redesign',
  description: 'Delaval Knight - Description',
};

export default function Index({ data }){
  return (
    <Layout metadata={Metadata}>
      <Redesign imageData={data.redesign} />
    </Layout>
  );
};

export const query = graphql`
  query {
    redesign: file(relativePath: { eq: "redesign.jpg" }) {
      childImageSharp {
        fixed(width: 650, height: 461, quality: 100) {
          ...GatsbyImageSharpFixed
        }
      }
    }
  }
`;

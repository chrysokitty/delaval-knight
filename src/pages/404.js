import React from "react";

import Layout from "../layouts/general";

const Metadata = {
  title: 'Delaval Knight - 404',
  description: 'Delaval Knight - Page not found',
};

export default function NotFoundPage(){
  return (
    <Layout metadata={Metadata}>
      <h1>NOT FOUND</h1>
      <p>You just hit a route that doesn&#39;t exist... the sadness.</p>
    </Layout>
  );
};

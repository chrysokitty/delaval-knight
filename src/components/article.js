import React from 'react';

import ArticleText from './article-text';
import ArticleImage from './article-image';
import Divider from './divider';

const articleClass = 'article';
const reverseClass = 'article-reverse';
const fullwidthClass = 'article-fullwidth';
const portraitClass = 'article-portrait';

export default class Article extends React.Component {
  get className(){
    let temp = articleClass;
    
    if(
      this.props.variant === 'reverse'
        ||
      this.props.variant === 'fullwidth-reverse'
    ){
      temp += ` ${reverseClass}`;
    }

    if(
      this.props.variant === 'fullwidth'
        ||
      this.props.variant === 'fullwidth-reverse'
        ||
      !this.props.imageData
    ){
      temp += ` ${fullwidthClass}`;
    }

    if(
      this.props.variant === 'portrait'
        &&
      this.props.imageData
    ){
      temp += ` ${portraitClass}`;
    }
    
    return temp;
  }
  
  render(){
    return (
      <div className="article-wrapper" id={this.props.id}>
        {this.props.showDivider && (
          <Divider />
        )}
        <article className={this.className}>
          <ArticleText>{this.props.children}</ArticleText>
          {this.props.imageData && (
            <ArticleImage imageData={this.props.imageData} />
          )}
        </article>
      </div>
    );
  }
}

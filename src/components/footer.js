import React from 'react';

import SocialLinks from '../utils/social-links';

export default class Footer extends React.Component {
  componentDidMount(){
    let social = new SocialLinks(document.querySelector('.footer-social'));

    // Enable service like this
    social.services.email.isUsed = false;
    social.services.facebook.isUsed = false;
    social.services.gplus.isUsed = false;
    social.services.linkedin.isUsed = false;
    social.services.print.isUsed = false;
    social.services.telephone.isUsed = false;
    social.services.twitter.isUsed = false;
    social.services.instagram.isUsed = true;

    // Render
    social.renderLinks({
      instagram: {
        url: 'https://www.instagram.com/delavalknightbespoke',
      }
    });
  }

  render(){
    return (
      <footer>
        <div className="footer-wrapper">
          <div className="social footer-social"></div>
          <div className="footer-nav">
            <div className="footer-nav-link">
              <a href="/delaval-knight/ethically-sourced-gems">Ethically sourced gems</a>
            </div>
            <div className="footer-nav-link">
              <a href="/delaval-knight/anti-slavery-policy">Anti Slavery Policy</a>
            </div>
            <div className="footer-nav-link">
              <a href="/delaval-knight/terms-and-conditions">Terms & Conditions</a>
            </div>
            <div className="footer-nav-link">
              <a href="/delaval-knight/privacy-policy">Privacy Policy</a>
            </div>
            <div className="footer-nav-link">
              <a href="/delaval-knight/shipping">Shipping Policy</a>
            </div>
            <div className="footer-nav-link">
              <a href="/delaval-knight/returns">Returns Policy</a>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

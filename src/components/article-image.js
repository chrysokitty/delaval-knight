import React from 'react';
import Img from "gatsby-image";

export default class Body extends React.Component {
  render(){
    const imageData = this.props.imageData;
    
    return (
      <div className="article-image">
        {imageData && (<Img {...imageData.childImageSharp} />)}
        {!imageData && (
          <div className="image-placeholder">
            Placeholder
          </div>
        )}
      </div>
    );
  }
}

import React from 'react';

export default class Body extends React.Component {
  render(){
    return (
      <main>
        {this.props.markdown && (
          <div
            className="markdown"
            dangerouslySetInnerHTML={{ __html: this.props.markdown }}
          />
        )}
        {this.props.children}
      </main>
    );
  }
}

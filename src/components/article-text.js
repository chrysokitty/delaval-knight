import React from 'react';

export default class Body extends React.Component {
  render(){
    return (
      <div className="article-text">
        {this.props.children}
      </div>
    );
  }
}

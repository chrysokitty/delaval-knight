import React from "react";
import { graphql } from "gatsby"
import { Helmet } from "react-helmet";

import Header from "../components/header";
import Body from "../components/body";
import Footer from "../components/footer";
import Divider from '../components/divider';

import "./general.css";

export default function MarkdownLayout({ data }) {
  const { frontmatter, html } = data.markdownRemark;

  return (
    <>
      <Helmet>
        <title>{frontmatter.title}</title>
        <meta type="description" content={frontmatter.description} />
        <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,600;1,400;1,600&display=swap" rel="stylesheet" />
      </Helmet>
      
      <div className="app-wrapper">
        <Header />
        <Body markdown={html}>
          <Divider />
        </Body>
        <Footer />
      </div>
    </>
  );
}

export const pageQuery = graphql`
  query($slug: String!) {
    markdownRemark(frontmatter: { slug: { eq: $slug } }) {
      html
      frontmatter {
        slug
        title
      }
    }
  }
`

import React from "react";
import { Helmet } from "react-helmet";

import Header from "../components/header";
import Body from "../components/body";
import Footer from "../components/footer";
import Divider from '../components/divider';

import "./general.css";

export default function Layout({ metadata, children }){
  return (
    <>
      <Helmet>
        <title>{metadata.title}</title>
        <meta type="description" content={metadata.description} />
        <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@0,400;0,600;1,400;1,600&display=swap" rel="stylesheet" />
      </Helmet>
      
      <div className="app-wrapper">
        <Header />
        <Body>
          {children}
          <Divider />
        </Body>
        <Footer />
      </div>
    </>
  )
};

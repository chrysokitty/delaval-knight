---
slug: "/anti-slavery-policy"
title: Delaval Knight - Anti Slavery Policy
description: Modern slavery is a crime resulting in an abhorrent abuse of the human rights of vulnerable workers. It can take various forms, such as slavery, servitude, forced or compulsory labour and human trafficking. 
---

# ANTI SLAVERY POLICY

Modern slavery is a crime resulting in an abhorrent abuse of the human rights of vulnerable workers. It can take various forms, such as slavery, servitude, forced or compulsory labour and human trafficking.

Delaval Knight Ltd has a zero-tolerance approach to modern slavery and is committed to acting ethically and with integrity and transparency in all of its business dealings and relationships and to implementing and enforcing effective systems and controls to ensure that modern slavery and human trafficking are not taking place anywhere within either its own business or in any of its supply chains, consistent with its obligations under the Modern Slavery Act 2015.

## SUPPLIERS

Delaval Knight Ltd is a private jewellery business, selling luxury jewellery and offering services such as; jewellery repairs, valuations and bespoke jewellery design.

Delaval Knight Ltd expects the same high standards from all of its suppliers, contractors and other business partners and expects that its suppliers will in turn hold their own suppliers to the same standards.

All suppliers are required to operate in an ethical and responsible manner, respecting all applicable laws and such laws relating to the Modern Slavery and Human Trafficking. They are required to treat their employees fairly throughout the supply chain; offering good working conditions and reasonable rates of pay.

Delaval Knight Ltd will only purchase from reputable suppliers who can guarantee the materials are ethically sourced. We will always try to work with suppliers who are members of the Responsible Jewellery Council and our diamond suppliers are required to state the diamonds are conflict free and abide by the Kimberly Process before we proceed in any business.

Our Buying team is trained in our requirements and will always consider the expectations when exploring new suppliers.

Suppliers are expected to notify Delaval Knight Ltd immediately if they become aware of Modern Slavery or any breach of Human Rights within their organisation or supply chains.

## RESPONSIBILITY AND COMPLIANCE

The prevention, detection and reporting of modern slavery in any part of the Company’s business or supply chains, whether in the UK or abroad, is the responsibility of all those working for the Company or under the Company’s control.

The Company Directors have overall responsibility for ensuring that this statement complies with the Company’s legal and ethical obligations and line managers are responsible for ensuring that those reporting to them understand and comply.

## BREACH OF THE POLICY

The Company may terminate its commercial relationship with suppliers, contractors and other business partners if they breach this policy and/or are found to have been involved in modern slavery.

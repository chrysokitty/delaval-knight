---
slug: "/returns"
title: Delaval Knight - Returns Policy
description: As our Jewellery orders are tailored specially for you, we will not be able to offer any direct returns. Although we have every confidence that your order will be loved for years to come, we will however be pleased to offer a re-design option for a fee. Each re-design fee will be reviewed on a case by case basis.
---

# Returns Policy

As our Jewellery orders are tailored specially for you, we will not be able to offer any direct returns. Although we have every confidence that your order will be loved for years to come, we will however be pleased to offer a re-design option for a fee. Each re-design fee will be reviewed on a case by case basis.

In accordance with UK Health and Hygiene regulations, unfortunately we cannot accept the return of earrings. This does not affect your statutory rights.

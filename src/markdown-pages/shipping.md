---
slug: "/shipping"
title: Delaval Knight - Shipping Policy
description: This policy contains details of the delivery methods, periods and charges that apply to orders for our products ordered by telephone or email.
---

# Shipping Policy

## Introduction

This policy contains details of the delivery methods, periods and charges that apply to orders for our products ordered by telephone or email. 

This policy does not create legally enforceable rights and obligations; it merely indicates our usual practice in relation to the delivery of products.

## Geographical limitations

We deliver to the following countries and territories: England, Scotland, Wales and Northern Ireland.
We may from time to time agree to deliver products to other countries and territories. Please contact us directly if you wish for us to ship an item to a country outside of the UK.

## Delivery methods and periods

Average dispatch time is usually within 2-3 working days of processing the order.

If your delivery address is on the United Kingdom mainland, Goods will be sent out by Special Delivery with Royal Mail and the typical period for delivery of products by this method is next working day. If your delivery address is outside of UK mainland, Goods will be sent with an alternative Courier service.

Estimated delivery times:

- UK Mainland; next working day 
- Europe; 3 – 5 working days
- Rest of World; 5 – 7 

If you place your order before 3pm on a working day, these time periods run from the close of business on that day; if you place your order after 3pm on a working day, or on a non-working day, these time periods run from the close of business on the next following working day. 

The delivery periods are indicative only, and whilst we will make every effort to ensure that you receive your delivery in good time, we do not guarantee delivery before the end of the stated period.

## Delivery charges

Delivery charges will be applied to your order during the checkout process. Delivery tracking is available in respect of all orders for our products. To track your delivery, enter your order number (which is provided in your order confirmation email) into the delivery service provider's website.

## Receipt and signature

All deliveries must be received in person at the delivery address, and a signature must be provided. If no one is present at specified address, the Courier will leave details on the premises with instructions on how you may obtain your package at a later date. Packages must be opened in presence of Courier upon receipt to verify Goods are undamaged. 

## Delivery problems

If you experience any problems with a delivery, please contact us using the contact details that we publish on our website. 

If our delivery service provider is unable to deliver your products and such failure is your fault, and you do not collect your products from our delivery service provider within the relevant time limit, we may agree to arrange for re-delivery of the products; however, we reserve the right to charge you for the actual costs of re-delivery.

An indicative list of the situations where a failure to deliver will be your fault is set out below: 

(a) you provided the wrong address for delivery;

(b) there is a mistake in the address for delivery that was provided;

(c) the address for delivery is not reasonably accessible;

(d) the address for delivery cannot safely be accessed;

(f) if in-person receipt is required, there is no person available at the address for delivery to accept delivery and provide a signature.

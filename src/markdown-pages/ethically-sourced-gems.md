---
slug: "/ethically-sourced-gems"
title: Delaval Knight - Ethically Sourced Gems
description: We share your concerns about ethically produced products and we do our utmost to ensure that the products we supply come from trusted and ethical suppliers. 
---

# ETHICALLY SOURCED GEMS

We share your concerns about ethically produced products and we do our utmost to ensure that the products we supply come from trusted and ethical suppliers.

We have long-standing relationships with our suppliers and have received assurances that they treat their workers fairly - providing a safe workplace, access to health care and a fair wage.

All of our stones are absolutely genuine semi-precious or precious gemstones and are certified. All items comply with hallmarking laws where applicable and comply with EU nickel regulations.

---
slug: "/terms-and-conditions"
title: Delaval Knight - Terms and Conditions
description: Delaval Knight Terms and Conditions
---

# TERMS AND CONDITIONS 

1. **INTRODUCTION**
    1. This disclaimer shall govern your use of our website.
    2. By using our website, you accept this disclaimer in full; accordingly, if you disagree with this disclaimer or any part of this disclaimer, you must not use our website.
    3. Our website uses cookies; by using our website or agreeing to this disclaimer, you consent to our use of cookies in accordance with the terms of our privacy policy.

2. **COPYRIGHT NOTICE**
    1. Copyright (c) 2020 Delaval Knight
    2. Subject to the express provisions of this disclaimer:
        1. we, together with our licensors, own and control all the copyright and other intellectual property rights in our website and the material on our website; and
        2. all the copyright and other intellectual property rights in our website and the material on our website are reserved.
 
3. **LICENCE TO USE WEBSITE**
    3. You may:
        1. view pages from our website in a web browser;
        2. download pages from our website for caching in a web browser;
        3. print pages from our website;
        4. stream audio and video files from our website; and
        5. use out website services by means of a web browser, subject to the other provisions of this disclaimer.
    2. Except as expressly permitted by Section 3.1 or the other provisions of this disclaimer, you must not download any material from our website or save any such material to your computer.
    3. You may only use our website for your own personal and business purposes, and you must not use our website for any other purposes.
    4. Unless you own or control the relevant rights in the material, you must not:
        1. republish material from our website (including republication on another website);]
        2. sell, rent or sub-license material from our website;
        3. show any material from our website in public;
        4. exploit material from our website for a commercial purpose; or
        5. redistribute material from our website.
    5. We reserve the right to restrict access to areas of our website, or indeed our whole website, at our discretion; you must not circumvent or bypass, or attempt to circumvent or bypass, any access restriction measures on our website.
 
4. **ACCEPTABLE USE**
    1. You must not:
        1. use our website in any way or take any action that causes, or may cause, damage to the website or impairment of the performance, availability or accessibility of the website;
        2. use our website in any way that is unlawful, illegal, fraudulent or harmful, or in connection with any unlawful, illegal, fraudulent or harmful purpose or activity;
        3. use our website to copy, store, host, transmit, send, use, publish or distribute any material which consists of (or is linked to) any spyware, computer virus, Trojan horse, worm, keystroke logger, rootkit or other malicious computer software;
        4. conduct any systematic or automated data collection activities (including without limitation scraping, data mining, data extraction and data harvesting) on or in relation to our website without our express written consent;
        5. access or otherwise interact with our website using any robot, spider or other automated means;
        6. violate the directives set out in the robots.txt file for our website; or
        7. use data collected from our website for any direct marketing activity (including without limitation email marketing, SMS marketing, telemarketing and direct mailing).
    2. You must not use data collected from our website to contact individuals, companies or other persons or entities.
    3. You must ensure that all the information you supply to us through our website, or in relation to our website, is \[true, accurate, current, complete and non-misleading].

5. **LIMITED WARRANTIES**
    1. We do not warrant or represent:
        1. the completeness or accuracy of the information published on our website;
        2. that the material on the website is up to date; or
        3. that the website or any service on the website will remain available.
    2. We reserve the right to discontinue or alter any or all of our website services, and to stop publishing our website, at any time in our sole discretion without notice or explanation; and save to the extent expressly provided otherwise in this disclaimer, you will not be entitled to any compensation or other payment upon the discontinuance or alteration of any website services, or if we stop publishing the website.
    3. To the maximum extent permitted by applicable law and subject to Section 6.1, we exclude all representations and warranties relating to the subject matter of this disclaimer, our website and the use of our website.

6. **LIMITATIONS AND EXCLUSIONS OF LIABILITY**
    1. Nothing in this disclaimer will:
        1. limit or exclude any liability for death or personal injury resulting from negligence;
        2. limit or exclude any liability for fraud or fraudulent misrepresentation;
        3. limit any liabilities in any way that is not permitted under applicable law; or
        4. exclude any liabilities that may not be excluded under applicable law.
    2. The limitations and exclusions of liability set out in this Section 6 and elsewhere in this disclaimer: 
        1. are subject to Section 6.1; and
        2. govern all liabilities arising under this disclaimer or relating to the subject matter of this disclaimer, including liabilities arising in contract, in tort (including negligence) and for breach of statutory duty.
    3. To the extent that our website and the information and services on our website are provided free of charge, we will not be liable for any loss or damage of any nature.
    4. We will not be liable to you in respect of any losses arising out of any event or events beyond our reasonable control.
    5. We will not be liable to you in respect of any business losses, including (without limitation) loss of or damage to profits, income, revenue, use, production, anticipated savings, business, contracts, commercial opportunities or goodwill.
    6. We will not be liable to you in respect of any loss or corruption of any data, database or software.
    7. We will not be liable to you in respect of any special, indirect or consequential loss or damage.
 
7. **VARIATION**
    1. We may revise this disclaimer from time to time.
    2. The revised disclaimer shall apply to the use of our website from the time of publication of the revised disclaimer on the website. 
 
8. **SEVERABILITY**
    1. If a provision of this disclaimer is determined by any court or other competent authority to be unlawful and/or unenforceable, the other provisions will continue in effect.
    2. If any unlawful and/or unenforceable provision of this disclaimer would be lawful or enforceable if part of it were deleted, that part will be deemed to be deleted, and the rest of the provision will continue in effect. 
 
9. **LAW AND JURISDICTION**
    1. This disclaimer shall be governed by and construed in accordance with English law.
    2. Any disputes relating to this disclaimer shall be subject to the exclusive jurisdiction of the courts of England.
 
10. **STATUTORY AND REGULATORY DISCLOSURES**
    1. Our VAT number is GB 352518113
    2. Company Number: 12731312
 
11. **OUR DETAILS**
    1. This website is owned and operated by Delaval Knight
    4. You can contact us by by email to [info@delavalknight.com](mailto:info@delavalknight.com) or phone [07876493377](tel:07876493377)

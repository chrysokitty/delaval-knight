---
slug: "/privacy-policy"
title: Delaval Knight - Privacy Policy
description: We are committed to safeguarding the privacy of our website visitors; in this policy we explain how we will treat your personal information. By using our website and agreeing to this policy, you consent to our use of cookies in accordance with the terms of this policy.
---

# Privacy Policy

## INTRODUCTION

We are committed to safeguarding the privacy of our website visitors; in this policy we explain how we will treat your personal information. By using our website and agreeing to this policy, you consent to our use of cookies in accordance with the terms of this policy.

## COLLECTING PERSONAL INFORMATION

We may collect, store and use the following kinds of personal information:

(a) Information that you provide to us for the purpose of booking an appointment.

## USING PERSONAL INFORMATION

Personal information submitted to us through our website will be used for the purposes specified in this policy.
We may use your personal information to:

(a) Send you goods purchased through our website.

(b) Send statements, invoices and payment reminders to you, and collect payments from you.

(c) Send you email notifications that you have specifically requested.

(d) Send you marketing communications relating to our business which we think may be of interest to you, by post or, where you have specifically agreed to this, by email or similar technology (you can inform us at any time if you no longer require marketing communications).

We will not, without your express consent, supply your personal information to any third party for the purpose of their or any other third party's direct marketing.

## DISCLOSING PERSONAL INFORMATION

We may disclose your personal information to our employees insofar as reasonably necessary for the purposes set out in this policy.
We may disclose your personal information:

(a) To the extent that we are required to do so by law;

(b) In connection with any ongoing or prospective legal proceedings;

(c) In order to establish, exercise or defend our legal rights (including providing information to others for the purposes of fraud prevention and reducing credit risk);

Except as provided in this policy, we will not provide your personal information to third parties.
 
## SECURITY OF PERSONAL INFORMATION

We will take reasonable technical and organisational precautions to prevent the loss, misuse or alteration of your personal information.
All personal information you provide is stored on our website platform providers system.

You acknowledge that the transmission of information over the internet is inherently insecure, and we cannot guarantee the security of data sent over the internet.

import React from 'react';

import Article from '../components/article';

export default function({ imageData }){
  return (
    <Article id="milestones" imageData={imageData}>
      <h1>Mile'stones'</h1>
      <p>Anniversaries, birthdays or the arrival of a new addition to the family are the perfect time to design something thoughtful for your loved one. Del is happy to work with any price level that you feel comfortable to create wonderful gifts to mark any important moment in your life.</p>
    </Article>
  );
}

import React from 'react';

import Article from '../components/article';

export default function({ imageData }){
  return (
    <Article id="about-us" variant="portrait" imageData={imageData}>
      <h1>About</h1>
      <p>Designing a unique piece of jewellery at any price point. This has been the driving force behind the creation of Delaval Knight Bespoke Jewellery in 2020.</p>
      <p>Having worked in the Fine Art and Jewellery world for over ten years, Delaval 'Del' Knight (a trained GIA Gemologist and British Jewellery Designer) spotted a gap in the market for a bespoke jeweler that you can trust to create quality pieces, keenly priced.</p>
      <p>Del takes great pleasure in sourcing the highest quality stones available on the market for his clients. The process of choosing gems, designs and thinking carefully about the occasion for which an item is created is all part of the exciting bespoke approach.</p>
      <p>Inspired by the antique jewellery of his family and all things Art Deco, Del designs vintage inspired contemporary pieces that are truly timeless.</p>
      <p>Get in touch at <a href="mailto:info@delavalknight.com">info@delavalknight.com</a></p>
    </Article>
  );
}

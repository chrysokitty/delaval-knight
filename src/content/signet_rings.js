import React from 'react';

import Article from '../components/article';

export default function({ imageData }){
  return (
    <Article id="signet-rings" imageData={imageData}>
      <h1>Signet Rings</h1>
      <p>No matter your background, signet rings celebrate your identity. From a family coat of arms to something more inventive, Del works carefully to ensure that the perfect combination and style is masterminded to suit the individual.</p>
    </Article>
  );
}

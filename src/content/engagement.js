import React from 'react';

import Article from '../components/article';

export default function({ imageData }){
  return (
    <Article id="engagement" imageData={imageData}>
      <h1>Engagement</h1>
      <p>Choosing an engagement ring should be an exciting process, yet many of us are daunted by the sheer number of options available. With a G&T to calm your nerves, Del will guide you through the many options ensuring that the final result is truly spectacular, whatever the budget.</p>
    </Article>
  );
}

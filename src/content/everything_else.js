import React from 'react';

import Article from '../components/article';

export default function({ imageData }){
  return (
    <Article id="everything-else" imageData={imageData}>
      <h1>Everything else</h1>
      <p>Whether you are missing that special pair of sapphire earrings to match the engagement ring, or a beautiful emerald bracelet to match an existing pendant, we are delighted to use our extensive knowledge to create any item of jewellery that you desire. There are no limits.</p>
    </Article>
  );
}

import React from 'react';

import Article from '../components/article';

export default function({ imageData }){
  return (
    <Article id="bespoke" variant="fullwidth-reverse" imageData={imageData}>
      <h1>Bespoke</h1>
      <h2>The 6 step unique bespoke process of Delaval Knight.</h2>
      <ol className="article-steps">
        <li>Answer a few short questions so we can prepare preliminary concepts. Quick, easy and our initial consultation comes with zero financial obligations.</li>
        <li>After our initial chat we will prepare several different design routes for you to consider. Once you’ve chosen the route, we can move onto the exciting task of selecting gems if appropriate and finalising the design using watercolour illustrations and CAD design (a 360 degree virtual computer image) to ensure you are totally happy with the design.</li>
        <li>You pay a 50% deposit to secure the deadline in our workshop’s busy schedule.</li>
        <li>Your jewellery is expertly handcrafted in our workshop by our experienced artisans with over forty years of experience in the industry.</li>
        <li>Your jewellery is ready! You are issued with any relevant certification and insurance valuations for your records.</li>
        <li>P.S. It doesn't stop there, our jewellery MOT service will help you to make the most of your jewellery for years to come.</li>
      </ol>
    </Article>
  );
};

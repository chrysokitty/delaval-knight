import React from 'react';

import Article from '../components/article';

export default function({ imageData }){
  return (
    <Article id="cufflinks" imageData={imageData}>
      <h1>Cufflinks</h1>
      <p>Tailored suit, polished shoes, crisp shirt; bespoke cufflinks are the perfect addition for any well dressed gentleman. Whether you desire classic and subtle sterling silver for the everyday or something more ostentatious for a party, Del takes great pleasure in helping you design the best fitting pair.</p>
    </Article>
  );
}

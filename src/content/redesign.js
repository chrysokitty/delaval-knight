import React from 'react';

import Article from '../components/article';

export default function({ imageData }){
  return (
    <Article id="redesign" imageData={imageData}>
      <h1>Redesign</h1>
      <p>You have a piece of jewellery that’s not your style, damaged or perhaps gifted from a relative. Del is passionate about remodeling Jewellery (whether vintage or modern) to ensure that it is perfect for the individual. It does not have to be expensive to make a world of difference.</p>
    </Article>
  );
}

import React from 'react';

import Article from '../components/article';

export default function({ imageData }){
  return (
    <Article id="wedding" imageData={imageData}>
      <h1>Wedding</h1>
      <p>The build up to a wedding can be hectic and financially challenging, Del is here to make your life easier. Whether your preference is Yellow, Rose or White Gold, with diamonds or without, the perfect ring for your special day is an essential.</p>
    </Article>
  );
}
